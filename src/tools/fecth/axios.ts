import { apiURL } from "../../config/api";
import axios from "axios";

axios.interceptors.request.use((config: any) => {
  config.baseURL = apiURL;
  config.validateStatus = (_: any) => true;
  config.headers = {
    Accept: "application/json",
    // "Content-Type": "application/json",
  };
  return Promise.resolve(config);
});

export default axios;
