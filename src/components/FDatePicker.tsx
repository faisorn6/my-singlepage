import { DatePicker } from "antd";
import FloatLabel from "./InputFieldCustom";

const FDatePicker = (props: any) => {
  return (
    <FloatLabel label={props.label || ""} name={props.id} value={props?.value}>
      <DatePicker
        {...props}
        bordered={false}
        onChange={props.onChange}
        disabled={props.disabled}
        placeholder={""}
        style={{
          width: "100%",
          marginBottom: "1rem",
        }}
      />
    </FloatLabel>
  );
};

export default FDatePicker;
