import { Input } from "antd";
import FloatLabel from "./InputFieldCustom";

const FInput = (props: any) => {
  return (
    <FloatLabel label={props.label || ""} name={props.id} value={props?.value}>
      <Input
        {...props}
        autoFocus
        bordered={false}
        style={{
          borderBottom: "1px solid #F5F5F5",
          borderRadius: 0,
          marginBottom: "1rem",
        }}
      />
    </FloatLabel>
  );
};

export default FInput;
