import { useState } from "react";

const FloatLabel = (props: any) => {
  const [focus, setFocus] = useState(false);
  const [valueBlur, setValueBlur] = useState("");
  const { children, label, value } = props;

  const labelClass =
    focus || (value && value.length !== 0) || valueBlur
      ? focus
        ? "label label-float-focused"
        : "label label-float"
      : "label-custom-float";

  return (
    <div
      id="float"
      className="float-label"
      onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        setValueBlur(newValue);
        setFocus(false);
      }}
      onFocus={() => setFocus(true)}
    >
      <label className={labelClass}>{label}</label>
      {children}
    </div>
  );
};

export default FloatLabel;
