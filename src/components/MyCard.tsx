import { Saffon_ride } from "../assets/image";

interface IMyCard {
  title?: string;
  text?: string;
  img?: string;
}

const MyCard = ({ title = "", text = "", img = Saffon_ride }: IMyCard) => {
  return (
    <div className="my-card-box">
      <img
        src={img}
        alt="img-card"
        width={"100%"}
        height={185}
        style={{ borderRadius: "5px 5px 0 0" }}
      />
      <div
        style={{
          padding: "1rem",
          backgroundColor: "white",
          borderRadius: "0 0 5px 5px",
          height: 120,
        }}
      >
        <div style={{ fontSize: 17, fontWeight: "bold", marginBottom: "1rem" }}>
          {title}
        </div>
        <div
          className="text-profile-overflow"
          style={{ fontSize: 13, marginBottom: "1.5rem" }}
        >
          {text}
        </div>
      </div>
    </div>
  );
};

export default MyCard;
