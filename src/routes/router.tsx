import React from "react";
import { RouteObject } from "react-router-dom";
import SinglePage from "../views/SinglePage";

export interface IRouter {
  path: string;
  name: string;
  authentMenuName: string;
  exact: boolean;
  component: React.FC;
}

export const routers: RouteObject[] = [
  {
    path: "/",
    element: <SinglePage />,
    index: true,
  },
];
