import axios, { AxiosResponse } from "axios";
import { useQuery, UseQueryResult } from "react-query";

export const thrownResponse = (res: AxiosResponse): never => {
  const message = `${res?.data?.message || "ดำเนินการไม่สำเร็จ"}`;
  throw message;
};

export const GET_PRODUCT = (params?: {}): UseQueryResult<any> => {
  return useQuery(["get-product", params], async () => {
    const res = await axios.get(`/products`, {
      params: { limit: 3, ...params },
    });
    if (res.data) return res.data;
    thrownResponse(res);
  });
};
