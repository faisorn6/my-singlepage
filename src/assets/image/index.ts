import LogoDino from "./kindpng_546104.png";
import ByCicleIcon from "./8-2-bicycle-png-8.png";
import MechaMan from "./MHMCEN.jpg";
import MechaMan2 from "./MHMCEN2.jpg";
import MechaMan3 from "./MHMCEN3.jpg";
import Saffon_ride from "./bisou_saffron_ride.jpg";

export { LogoDino, ByCicleIcon, MechaMan, MechaMan2, MechaMan3, Saffon_ride };
