import { Col, Row } from "antd";
import MyCard from "../../components/MyCard";
import { Saffon_ride } from "../../assets/image";
import FadeInSection from "../../components/ScrollFade";

export interface IItem {
  id: number;
  title: string;
  description: string;
  price: number;
  discountPercentage: number;
  rating: number;
  stock: number;
  brand: string;
  category: string;
  thumbnail: string;
  images: string[];
}

export interface ISectionProps {
  list: IItem[];
}

const SectionThree = ({ list = [] }: ISectionProps) => {
  return (
    <div style={{ backgroundColor: "#D9EF0C", padding: "6rem 1rem" }}>
      <Row
        justify={"center"}
        style={{ color: "black", fontSize: 45, fontWeight: "bold" }}
      >
        LOREM IPSUM
      </Row>
      <FadeInSection>
        <Row
          justify={"center"}
          style={{
            margin: "2rem 0",
          }}
          className={"product-overflow"}
        >
          {list?.map((i, idx: number) => {
            return (
              <Col key={idx} style={{ margin: "0 0.6rem" }}>
                <MyCard
                  title={i?.title || ""}
                  text={i?.description || ""}
                  img={i?.thumbnail || Saffon_ride}
                />
              </Col>
            );
          })}
        </Row>
      </FadeInSection>
    </div>
  );
};

export default SectionThree;
