import { Col, Row } from "antd";
import { ByCicleIcon } from "../../assets/image";
import FadeInSection from "../../components/ScrollFade";

const SectionOne = () => {
  // <FadeInSection /> is effect fade Up components
  return (
    <FadeInSection>
      <Row
        justify={"center"}
        align={"middle"}
        style={{ padding: "8rem 0 6rem 0", margin: 0 }}
        gutter={[30, 40]}
      >
        <Col xl={5} lg={10} md={10} className="text-info-section-one">
          <div style={{ fontSize: 50, fontWeight: "bold", color: "white" }}>
            LOREM
          </div>
          <div style={{ fontSize: 50, fontWeight: "bold", color: "#D9EF0C" }}>
            IPSUM
          </div>
          <div
            style={{
              fontSize: 14,
              color: "white",
              marginTop: 25,
              fontWeight: "bold",
              lineHeight: 1.5,
            }}
          >
            Lorem Ipsum is simply Lorem Ipsum is simply Lorem Ipsum is simply
          </div>
          <div
            style={{
              fontSize: 12,
              color: "white",
              marginTop: 15,
              fontWeight: "lighter",
              lineHeight: 1.5,
            }}
          >
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's
          </div>
        </Col>
        <Col xl={9} lg={12} md={12}>
          <img src={ByCicleIcon} alt="bicycle" width={"100%"} height={"100%"} />
        </Col>
      </Row>
    </FadeInSection>
  );
};

export default SectionOne;
