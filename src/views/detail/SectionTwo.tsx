import { useEffect } from "react";
import { MechaMan, MechaMan2, MechaMan3 } from "../../assets/image";
import { Button, Col, Row } from "antd";
import FadeInSection from "../../components/ScrollFade";

const list = [
  { title: "Lorem Ipsum is simply 1", image: MechaMan },
  { title: "Lorem Ipsum is simply 2", image: MechaMan2 },
  { title: "Lorem Ipsum is simply 3", image: MechaMan3 },
];

const SectionTwo = () => {
  let slideIndex = 1;

  useEffect(() => {
    SlickSlide(slideIndex);
  });

  const plusDivs = (n: number) => {
    SlickSlide((slideIndex += n));
  };

  const SlickSlide = (n: number) => {
    let i;
    let x = document.getElementsByClassName("mySlides");
    if (n > x.length) {
      slideIndex = 1;
    }
    if (n < 1) {
      slideIndex = x.length;
    }
    for (i = 0; i < x.length; i++) {
      x[i].className = "mySlides none";
    }
    x[slideIndex - 1].className = "mySlides block";
  };

  const SlideBtn = () => {
    return (
      <Row
        justify={"space-between"}
        align={"middle"}
        style={{
          position: "absolute",
          width: "100%",
          top: "50%",
          left: 0,
          padding: "0 15px",
        }}
      >
        <Col>
          <Button
            className="slide-btn"
            onClick={() => {
              plusDivs(-1);
            }}
          >
            &#10094;
          </Button>
        </Col>
        <Col>
          <Button
            className="slide-btn"
            onClick={() => {
              plusDivs(+1);
            }}
          >
            &#10095;
          </Button>
        </Col>
      </Row>
    );
  };

  const SlideContent = ({ title, image }: { title: string; image: string }) => {
    return (
      <div className="mySlides">
        <Row justify={"center"} style={{ paddingBottom: 20 }}>
          <Col className="white-font" style={{ paddingRight: 8 }}>
            LOREM IPSUM
          </Col>
          <Col className="lemon-font">LOREM</Col>
        </Row>
        <Row justify={"center"} style={{ marginTop: "4rem" }}>
          <img
            src={image || MechaMan}
            alt="icon-people"
            width={105}
            height={100}
            style={{
              borderRadius: "50%",
              objectFit: "cover",
            }}
          />
        </Row>
        <div
          className="text-center"
          style={{
            fontSize: 16,
            color: "white",
            marginTop: "2.5rem",
            fontWeight: "bold",
          }}
        >
          {title || ""}
        </div>
        <Row justify={"center"}>
          <div
            style={{
              fontSize: 12,
              color: "white",
              marginTop: 15,
              width: 500,
              fontWeight: "lighter",
              lineHeight: 1.5,
            }}
            className="text-center"
          >
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.
          </div>
        </Row>
      </div>
    );
  };

  return (
    <FadeInSection>
      <div style={{ position: "relative", padding: "6rem 1rem 8rem 1rem" }}>
        {list?.map((item: { title: string; image: string }, idx: number) => (
          <SlideContent key={idx} title={item?.title} image={item?.image} />
        ))}
        <SlideBtn />
      </div>
    </FadeInSection>
  );
};

export default SectionTwo;
