import { Row, Col, Form, Button } from "antd";
import { ByCicleIcon } from "../../assets/image";
import { useForm } from "antd/es/form/Form";
import FInput from "../../components/FInput";
import FDatePicker from "../../components/FDatePicker";
import FadeInSection from "../../components/ScrollFade";

const SectionFour = ({ onFinish }: { onFinish: (values: any) => void }) => {
  const [form] = useForm();

  const OnFinish = (values: any) => {
    const result = { ...values, date: values?.date?.format("DD/MM/YYYY") };
    onFinish(result);
  };

  return (
    <FadeInSection>
      <Row
        justify={"center"}
        align={"middle"}
        style={{ margin: "6rem 0" }}
        gutter={60}
      >
        <Col xl={10} lg={12} md={12} sm={24}>
          <img src={ByCicleIcon} alt="bicycle" className="icon-bycicle" />
        </Col>
        <Col xl={6} lg={9} md={10} sm={24}>
          <div
            style={{
              color: "#FFFFFF",
              fontSize: 35,
              fontWeight: "bold",
              marginBottom: "2rem",
            }}
          >
            ติดต่อเรา
          </div>
          <Form
            form={form}
            onFinish={OnFinish}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item name="fullName">
              <FInput label="ชื่อ - นามสกุล" />
            </Form.Item>
            <Form.Item
              name="phone"
              rules={[
                {
                  pattern: new RegExp("^[0][0-9]{9,9}$"),
                  message: "คุณป้อนเบอร์โทรศัพท์ไม่ถูกต้อง!",
                },
              ]}
            >
              <FInput label="เบอร์ติดต่อ" />
            </Form.Item>
            <Form.Item
              name="email"
              rules={[
                {
                  pattern: new RegExp(
                    "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$"
                  ),
                  message: "คุณป้อนอีเมลไม่ถูกต้อง!",
                },
              ]}
            >
              <FInput label="อีเมล" />
            </Form.Item>
            <Form.Item name="date" style={{ marginBottom: 10 }}>
              <FDatePicker label="วันที่" />
            </Form.Item>
          </Form>
          <div style={{ color: "#FFFFFF", fontSize: 13, lineHeight: 1.5 }}>
            Lorem Ipsum is simply dummy text Lorem Ipsum is simply dummy te
          </div>
          <Button
            style={{ marginTop: "2rem" }}
            className="btn-submit"
            onClick={() => form?.submit()}
          >
            SUBMIT
          </Button>
        </Col>
      </Row>
    </FadeInSection>
  );
};

export default SectionFour;
