import { GET_PRODUCT } from "../services/api/product";
import SectionFour from "./detail/SectionFour";
import SectionOne from "./detail/SectionOne";
import SectionThree from "./detail/SectionThree";
import SectionTwo from "./detail/SectionTwo";

const Content = () => {
  const getDataAPI = GET_PRODUCT();

  const OnFinish = (values: any) => {
    console.log("OnFinish", values);
  };

  return (
    <div>
      <SectionOne />
      <SectionTwo />
      <SectionThree list={getDataAPI?.data?.products || []} />
      <SectionFour onFinish={OnFinish} />
    </div>
  );
};

export default Content;
