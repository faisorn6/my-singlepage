import { LogoDino } from "../assets/image";
import { Col, Layout, Row } from "antd";
import ContentPage from "./Content";

const { Header, Content } = Layout;

const menuMock = new Array(5).fill(null).map((_, index) => {
  return index % 2 === 1 ? "LOREMIPSUM" : "LOREM";
});

const SinglePage = () => {
  const NavMenu = () => {
    return (
      <Col xl={19} lg={18} md={16} sm={20} xs={24}>
        <Row gutter={30} className={"menu-overflow"}>
          {menuMock.map((menu, index: number) => {
            return (
              <Col key={index} className="menu-item">
                {menu}
              </Col>
            );
          })}
        </Row>
      </Col>
    );
  };

  const Logo = () => {
    return (
      <Col xl={4} lg={6} md={8} sm={4} xs={24}>
        <Row align={"middle"}>
          <img
            src={LogoDino}
            alt="logo"
            width={45}
            height={50}
            style={{ marginRight: 15 }}
          />
          <span
            style={{
              fontSize: 17,
              fontWeight: "bold",
              color: "#FFFFFF",
              paddingRight: 5,
            }}
          >
            LOREM
          </span>
          <span style={{ fontSize: 17, fontWeight: "bold", color: "#D9EF0C" }}>
            IPSUM
          </span>
        </Row>
      </Col>
    );
  };

  return (
    <Layout className="layout" style={{ backgroundColor: "black" }}>
      <Header style={{ backgroundColor: "black" }}>
        <Row gutter={10} align={"middle"}>
          <Logo />
          <NavMenu />
        </Row>
      </Header>
      <Content style={{ minHeight: "90vh" }}>
        <ContentPage />
      </Content>
    </Layout>
  );
};

export default SinglePage;
